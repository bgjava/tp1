/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import modele.ImpossibleMoveException;
import modele.Labyrinthe;
import tp1.TP1;

/**
 *
 * @author Louis
 */
public class LabyrintheGraphique extends JFrame implements ActionListener{
    Labyrinthe g_lab;
    JPanel PanelLab, PanelControle;
    Container contentPane;
    
    // En jeu
    JButton ButtonLeft, ButtonRight, ButtonTop, ButtonBottom, ButtonSmart, ButtonAlea, ButtonClavier;
    ImageIcon imagePersoBegin = new ImageIcon(this.getClass().getResource("/IMG/perso_begin.png"));
    ImageIcon imagePerso = new ImageIcon(this.getClass().getResource("/IMG/perso.png"));
    ImageIcon imagePersoEnd = new ImageIcon(this.getClass().getResource("/IMG/perso_end.png"));
    ImageIcon imageMur = new ImageIcon(this.getClass().getResource("/IMG/wall.png"));
    ImageIcon imageTrou = new ImageIcon(this.getClass().getResource("/IMG/grass.png"));
    ImageIcon imageBegin = new ImageIcon(this.getClass().getResource("/IMG/begin.png"));
    ImageIcon imageEnd = new ImageIcon(this.getClass().getResource("/IMG/end.png"));

    JLabel choiceLabel;
    JLabel arrowLabel;
    
    // Fin
    JPanel PanelFin;
    JButton ButtonRecomm, ButtonStop, ButtonNouveau;
    JLabel WinLabel;
    
    // Taille d'une case
    int tailleX;
    int tailleY;
    
    int delaiMs;
    
    /** Creates a new instance of EventFrame */
    public LabyrintheGraphique(String titre, Labyrinthe pobjet) {
        // Lab
        g_lab = pobjet; // g_lab prend la valeur du labyrinte
        delaiMs = 1;
        
        tailleX = (800/g_lab.getTailleX());
        tailleY = (800/g_lab.getTailleX());
        
        // Donner un titre
        setTitle(titre); // On modifie le titre de la fenêtre
        contentPane = getContentPane(); // On crée un container
        
        
        PanelLab = new JPanel(); // On instancie le PanelLab qui contient le labyrinthe
        PanelLab.setLayout(new GridLayout(g_lab.getTailleX(),g_lab.getTailleY())); // Le Panel contient TailleX * TailleY cases, et on applique un layout de type Grid
        
        affichage_glab(); // On affiche le labyrinthe une première fois

         /// PANELCONTROLE
        PanelControle = new JPanel();
        PanelControle.setLayout(null);
        
        
        ButtonSmart = new JButton("Intelligent");
        ButtonAlea = new JButton("Aléatoire");
        ButtonClavier = new JButton("Déplacement clavier");
        choiceLabel = new JLabel("Faites votre choix :");
        arrowLabel = new JLabel("Jouer manuellement :");
        
        
        ButtonSmart.addActionListener(this);
        ButtonAlea.addActionListener(this);
        ButtonClavier.addActionListener(this);
        
        ButtonSmart.setBounds(new Rectangle(15,160,120,40));
        ButtonAlea.setBounds(new Rectangle(145,160,120,40));
        ButtonClavier.setBounds(new Rectangle(40,240,210,40));
        choiceLabel.setBounds(new Rectangle(20,110,120,40));
        arrowLabel.setBounds(new Rectangle(20,310,150,40));
        
        PanelControle.add(choiceLabel);
        PanelControle.add(arrowLabel);
        PanelControle.add(ButtonSmart);
        PanelControle.add(ButtonAlea);
        PanelControle.add(ButtonClavier);
        
        // ON INSTANCIE TOUS LES BOUTONS
        ButtonLeft = new JButton("<");
        ButtonRight = new JButton(">");
        ButtonTop = new JButton("^");
        ButtonBottom = new JButton("v");
        
        // ON LEURS MET DES ACTIONLISTENER
        ButtonLeft.addActionListener(this);
        ButtonRight.addActionListener(this);
        ButtonTop.addActionListener(this);
        ButtonBottom.addActionListener(this);

        ButtonTop.setBounds(new Rectangle(104,350,70,40));
        ButtonBottom.setBounds(new Rectangle(104,460,70,40));
        ButtonLeft.setBounds(new Rectangle(20,405,70,40));
        ButtonRight.setBounds(new Rectangle(188,405,70,40));
        
        ButtonClavier.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
                System.out.println("Key pressed code=" + e.getKeyCode() + ", char=" + e.getKeyChar());
                if(e.getKeyCode() == KeyEvent.VK_LEFT) {
                    System.out.println("KEYPRESSED");
                    try {
                        g_lab.move('q');
                    } catch (ImpossibleMoveException ex) {
                        
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_DOWN) {
                    System.out.println("KEYPRESSED");
                    try {
                        g_lab.move('s');
                    } catch (ImpossibleMoveException ex) {
                        
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    System.out.println("KEYPRESSED");
                    try {
                        g_lab.move('d');
                    } catch (ImpossibleMoveException ex) {
                        
                    }
                }
                if(e.getKeyCode() == KeyEvent.VK_UP) {
                    System.out.println("KEYPRESSED");
                    try {
                        g_lab.move('z');
                    } catch (ImpossibleMoveException ex) {
                        
                    }
                }
                
                affichage_glab();
                checkIfWon();
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

//        addKeyListener(new KeyAdapter() {
//            public void keyPressed(KeyEvent e) {
//                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    System.out.println("KEYPRESSED");
//                    try {
//                        g_lab.move('q');
//                    } catch (ImpossibleMoveException ex) {
//                        Logger.getLogger(LabyrintheGraphique.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                    affichage_glab();
//                }
//            }
//        });
        
        // ON LES AJOUTE AU PANELBUTTON
        PanelControle.add(ButtonLeft);
        PanelControle.add(ButtonRight);
        PanelControle.add(ButtonTop);
        PanelControle.add(ButtonBottom);
        
        // ON PLACE LE PANELLAB A GAUCHE ET LE PANELBUTTON A DROITE
        contentPane.add(PanelLab,BorderLayout.WEST);
        contentPane.add(PanelControle,BorderLayout.EAST);
        
        // ON DIMENSIONNE LES DEUX PANELS
        //PanelLab.setPreferredSize(new Dimension(g_lab.getTailleY() * tailleY, g_lab.getTailleX() * tailleX));
        PanelLab.setPreferredSize(new Dimension((800/g_lab.getTailleX())*g_lab.getTailleY(), 800));
        PanelControle.setPreferredSize(new Dimension(282, 800));
        
        
        
        
        PanelFin = new JPanel(); // On instancie le panel qui contient les boutons
        PanelFin.setLayout(new FlowLayout()); // On applique un layout de type Flow
        
        WinLabel = new JLabel("Vous avez gagné !!!! Bravo !");
        ButtonRecomm = new JButton("Recommencer");
        ButtonStop = new JButton("Quitter");
        ButtonNouveau = new JButton("Charger un autre labyrinthe");

        ButtonRecomm.addActionListener(this);
        ButtonStop.addActionListener(this);
        ButtonNouveau.addActionListener(this);
        
        PanelFin.add(WinLabel);
        PanelFin.add(ButtonRecomm);
        PanelFin.add(ButtonStop);
        PanelFin.add(ButtonNouveau);

        contentPane.add(PanelFin, BorderLayout.CENTER);
        
        PanelFin.setVisible(false);
        // ON DIMENSIONNE LE PANEL
        //PanelFin.setPreferredSize(new Dimension(282, 800));
        
        
        
        // ON DIMENSIONNE LA FENETRE
        setSize((800/g_lab.getTailleX())*g_lab.getTailleY() + 290, 800);
        //setResizable(false);
        
        // ON LE REND VISIBLE
        setVisible(true);
        
        // ON STOP L'EXECUTABLE SI ON QUITTE LA FENETRE
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
        
    public void affichage_glab()
    {
        // ON VIDE LE PANELLAB DE TOUS SES ELEMENTS
        PanelLab.removeAll();
        
        
        // ON AFFICHE LE LABYRINTHE GRAPHIQUE EN INSTANCIANT TOUTES LES CASES (BUTTONS)
        for(int i=0; i<g_lab.getTailleX(); i++)
        {
            for(int j=0; j<g_lab.getTailleY(); j++)
            {
                if(i==g_lab.getDepartY() && j==g_lab.getDepartX()) // Si c'est le case départ
                {
                    if(g_lab.getCurrentPositionY() == g_lab.getDepartY() && g_lab.getCurrentPositionX() == g_lab.getDepartX()) // Si le joueur est sur la case départ
                    {
                        addToPanel(imagePersoBegin);
                    }
                    else // Si le joueur n'est pas sur la case départ
                    {
                        addToPanel(imageBegin);
                    }
                }
                else if(i==g_lab.getArriveeY() && j==g_lab.getArriveeX()) // Si c'est la case d'arrivée
                {
                    if(g_lab.getCurrentPositionY() == g_lab.getArriveeY() && g_lab.getCurrentPositionX() == g_lab.getArriveeX()) // Si le joueur est sur la case d'arrivée
                    {
                        addToPanel(imagePersoEnd);
                    }
                    else // Si le joueur est sur la case d'arrivée
                    {
                        addToPanel(imageEnd);
                    }
                }
                else // Si c'est ni la case de départ, ni la case d'arrivée (donc soit un trou soit un mur)
                {
                    if(g_lab.getTerrain()[i][j].getSymbole() == 'X') // S'il s'agit d'un mur
                    {
                            addToPanel(imageMur);
                    }
                    else // S'il s'agit d'un trou
                    {
                        if(j==g_lab.getCurrentPositionY() && i==g_lab.getCurrentPositionX()) // Si le joueur est sur le trou
                        {
                            addToPanel(imagePerso);
                        }
                        else // Si le joueur n'est pas sur le trou
                        {
                            addToPanel(imageTrou);
                        }
                        
                    }
                }
                
            }        
        }
        
        PanelLab.revalidate(); // On applique les changements dans le Panel
        PanelLab.repaint(); // On affiche ces changements
    }
    
    public void addToPanel(ImageIcon image){
        
        // Ajout d'un nouveau bouton au Layout
        Image myimage = image.getImage(); // transform it 
        Image newimg = myimage.getScaledInstance(tailleX, tailleY,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
        image = new ImageIcon(newimg);  // transform it back
        
        JButton newCase = new JButton("", image); // On crée un nouveau bouton avec comme image de fond "imageTrou"
        //newCase.setBorder(BorderFactory.createEmptyBorder()); // On enlève les bordures du bouton pour l'esthétique
        //newCase.setBounds(j * tailleX, i * tailleY, tailleX, tailleY);
        PanelLab.add(newCase); // On ajoute la case au panel
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == ButtonBottom){
            try
            {
                g_lab.move('s');
            }
            catch (ImpossibleMoveException ex) {
            }
            
            affichage_glab();
        }
        
        if (e.getSource() == ButtonTop){
            try 
            {
                g_lab.move('z');
            } 
            catch (ImpossibleMoveException ex) {
            }
            
            affichage_glab();
        }
        
        if (e.getSource() == ButtonLeft){
            try 
            {
                g_lab.move('q');
            } 
            catch (ImpossibleMoveException ex) {
            }
            
            affichage_glab();
        }
        
        if (e.getSource() == ButtonRight){
            try 
            {
                g_lab.move('d');
            } 
            catch (ImpossibleMoveException ex) {
            }
            affichage_glab();
        }
        
        if (e.getSource() == ButtonSmart){
            affichage_glab();
            Timer timer = new Timer(delaiMs, tEvent -> {
                g_lab.autoMove();
                affichage_glab();
            });
            timer.start();  
        }
        if (e.getSource() == ButtonAlea){
            Timer timer = new Timer(delaiMs, tEvent -> {
                try 
                {
                    g_lab.randomMove();
                } 
                catch (ImpossibleMoveException ex) 
                {
                    
                }
                affichage_glab();
                
                /// VICTOIRE -> IL FAUT AFFICHER 
                if (g_lab.getCurrentPositionX()==g_lab.getArriveeX() && g_lab.getCurrentPositionY()==g_lab.getArriveeY()) {
                    ((Timer) tEvent.getSource()).stop();
                    checkIfWon();
                }
                
            });
            timer.start();
        }
        
        // Fin de partie
        if (e.getSource() == ButtonRecomm) {
            
            // Reinitialiser les valeurs
            reinit();
            
            affichage_glab();
            
            PanelLab.setVisible(true);
            PanelControle.setVisible(true);
            PanelFin.setVisible(false);
        }
        if (e.getSource() == ButtonStop) {
            System.exit(0);
        }
        if (e.getSource() == ButtonNouveau) {
            try {
                g_lab = TP1.charger_lab();
            } catch (FileNotFoundException ex) {
            }
            
            PanelLab.setVisible(true);
            PanelControle.setVisible(true);
            PanelFin.setVisible(false);
        }
        
        checkIfWon();
    }
    
    public void reinit(){
        g_lab.setCurrentPositionX(g_lab.getDepartX());
        g_lab.setCurrentPositionY(g_lab.getDepartY());
    }
    
    /**
     *
     */
    public void checkIfWon(){
        if(g_lab.getCurrentPositionX() == g_lab.getArriveeX() && g_lab.getCurrentPositionY() == g_lab.getArriveeY())
        {
            PanelLab.setVisible(false);
            PanelControle.setVisible(false);
            PanelFin.setVisible(true);
        }
    }
}
