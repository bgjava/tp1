/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.awt.AWTException;
import java.io.IOException;
import modele.Labyrinthe;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe permet d'afficher à l'écran les textes du menu et le labyrinthe.
 * 
 * @see Labyrinthe
 * @author Philippe Ong
 */
public class Partie {
    
    /**
    * Ceci est le constructeur de Partie.
    */
    public Partie()
    {
        
    }
    
    /**
    * Ceci est la procédure affichant le labyrinthe.
    * 
    * @param lab
    *           Le labyrinthe à afficher.
    * @see Labyrinthe
    */
    public static void afficher_lab(Labyrinthe lab)
    {
        char playerSymbole = '*';
        char beginPoint = '>';
        char endPoint = 'o';
        for(int j=0; j<lab.getTailleX(); j++){
            
            System.out.println(" ");
            
            for(int i=0; i<lab.getTailleY(); i++){
                if(j == lab.getCurrentPositionX() && i == lab.getCurrentPositionY()){
                    System.out.print(playerSymbole);
                }
                else{
                    if(j == lab.getDepartX() && i == lab.getDepartY()){
                        System.out.print(beginPoint);
                    }
                    else{
                        if(j == lab.getArriveeX() && i == lab.getArriveeY()){
                            System.out.print(endPoint);
                        }
                        else{
                            System.out.print(lab.getTerrain()[j][i].getSymbole());
                        }
                    }
                }
            }
        }
        System.out.println(" ");
        System.out.println(" ");
    }

    /**
    * Ceci est la procédure affichant le menu.
    */
    public static void afficherMenu() {
        System.out.println("");
        System.out.println("Bienvenue dans le menu du jeu");
        System.out.println("LABYRINTHE");
        
        System.out.println("0. Jouer manuellement");
        System.out.println("1. Mode auto intelligent");
        System.out.println("2. Mode auto aléatoire");
        System.out.println("3. Quitter");
        
        System.out.println("Veuillez choisir : ");
    }
    
}