/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Interface de CaseImplementee
 * 
 * @see CaseImplementee
 * @author Philippe Ong
 */
public interface Case {
    public int getPositionX(); // Retourne la position en X de la case
    public int getPositionY(); // Retourne la position en Y de la case
    public boolean canMoveToCase(); // Indique si la case est accessible ou non
    public char getSymbole();
}
