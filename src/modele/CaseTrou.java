/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Une case Trou sur laquelle on peut se déplacer. C'est une fille de CaseImplementee.
 * 
 * @see CaseImplementee
 * @author Philippe Ong
 */
public class CaseTrou extends CaseImplementee{ // CaseTrou hérite de CaseImplementee
    /**
    * Constructeur de cette classe. Initialise les attributs.
    * 
    * @param X
    *           Position en X de la case Trou implémentée.
    * @param Y
    *           Position en Y de la case Trou implémentée.
    * @see CaseImplementee#getPositionX()
    */
    public CaseTrou(int X, int Y)
    {
        super(X,Y,true, '_'); // On appelle le constructeur de CaseImplementee en initialisant l'attribut "vasy" par true
    }
  
}
