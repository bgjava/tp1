/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author Louis
 */
public class Noeud {
    public int posX;
    public int posY;
    
    public Noeud(){
        posX = 0;
        posY = 0;
    }
    
    public Noeud(int x, int y){
        posX = x;
        posY = y;
    }
}
