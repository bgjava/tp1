/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Cette classe implémente Case qui est son interface.
 * 
 * @see Case
 */
public class CaseImplementee implements Case{
    
    /**
    * Position sur l'axe des X du joueur.
    * 
    * @see CaseImplementee#getPositionX()
    */
    protected int posX;
    
    /**
    * Position sur l'axe des Y du joueur.
    * 
    * @see CaseImplementee#getPositionY()
    */
    protected int posY;
    
    /**
    * Booleen donnant la possibilité ou non d'accéder à une case.
    * 
    * @see CaseImplementee#canMoveToCase()
    */
    protected boolean vasy;
    
    /**
    * Le symbole qui est affiché lorsqu'on souhaite afficher une case.
    * 
    * @see CaseImplementee#getSymbole()
    */
    protected char symbole;
    
    /**
    * Constructeur de la classe. Initialise les valeurs des arguments.
    * 
    * @param X
    *           Coordonnée de la case en abscisse.
    * 
    * @param Y
    *           Coordonnée de la case en ordonnée.
    * 
    * @param case_accessible 
    *           Booleen montrant si le joueur peut marcher sur la case ou non.
    * 
    * @param sym
    *           Symbole affiché au moment d'afficher la case.
    * 
    * @see CaseImplementee#canMoveToCase()
    */
    public CaseImplementee(int X, int Y, boolean case_accessible, char sym)
    {
        posX = X;
        posY = Y;
        vasy = case_accessible;
        symbole = sym;
    }
    
    /**
    * Fonction retournant la position en X du joueur.
    * 
    * @return posX, la position en X du joueur.
    */
    public int getPositionX() // Retourne la position en X de la case
    {
        return posX;
    }
    
    /**
    * Fonction retournant la position en Y du joueur.
    * 
    * @return posY, la position en Y du joueur.
    */
    public int getPositionY() // Retourne la position en Y de la case
    {
        return posY;
    }
    
    /**
    * Fonction retournant si le joueur peut se déplacer ou non sur la case spécifiée.
    * 
    * @return vasy, Booleen spécifiant si le joueur peut se déplacer sur cette case.
    */
    public boolean canMoveToCase() // Indique si la case est accessible ou non
    {
        return vasy;
    }
    
    /**
    * Fonction retournant le symbole (caractère) représentant la case à afficher.
    * 
    * @return symbole, le caractère représentant la case à afficher.
    */
    public char getSymbole()
    {
        return symbole;
    }
    
}
