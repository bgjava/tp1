/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Une case Mur sur laquelle on ne peut pas se déplacer. C'est une fille de CaseImplementee.
 * 
 * @see CaseImplementee
 * @author Philippe Ong
 */
public class CaseMur extends CaseImplementee{ // CaseMur hérite de CaseImplementee
    /**
    * Constructeur de cette classe. Initialise les attributs.
    * 
    * @param X
    *           Position en X de la case Mur implémentée.
    * @param Y
    *           Position en Y de la case Mur implémentée.
    * @see CaseImplementee#getPositionX()
    */
    public CaseMur(int X, int Y)
    {
        // On appelle le constructeur de CaseImplementee en initialisant
        // l'attribut "vasy" par false
        super(X, Y, false, 'X');
    }
    
}
