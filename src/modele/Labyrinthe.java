/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.io.File;
import java.io.FileNotFoundException;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

/**
 * <b>Labyrinthe est la classe représentant le labyrinthe générée par le fichier texte</b>
 * <p>
 * Un labyrinthe possède les attributs suivants :
 * <ul>
 * <li>Sa longueur</li>
 * <li>Sa largeur</li>
 * <li>Son point de départ (en X et en Y)</li>
 * <li>Son point d'arrivée (en X et en Y)</li>
 * <li>La position actuelle du personnage (en X et en Y)</li>
 * <li>Son corps constitué de cases (mur ou trou) </li>
 * </ul>
 * </p>
 * <p>
 * De plus, un Zéro a une liste d'amis Zéro. Le membre pourra ajouter ou enlever
 * des amis à cette liste.
 * </p>
 * 
 * @see CaseImplementee
 * 
 */

public class Labyrinthe {
    
    /// ATTRIBUTS
    
    /**
    * La longueur du labyrinthe
    * 
    * @see Labyrinthe#getTailleX()
    */
    private int tailleX;
    
    /**
    * La largeur du labyrinthe
    * 
    * @see Labyrinthe#getTailleY()
    */
    private int tailleY;
    
    /**
    * Le point de départ du labyrinthe en X
    * 
    * @see Labyrinthe#getDepartX()
    */    
    private int departX;
    
    /**
    * Le point de départ du labyrinthe en Y
    * 
    * @see Labyrinthe#getDepartY()
    */ 
    private int departY;
    
    /**
    * Le point d'arrivée du labyrinthe en X
    * 
    * @see Labyrinthe#getArriveeX()
    */ 
    private int arriveeX;
    
    /**
    * Le point d'arrivée du labyrinthe en Y
    * 
    * @see Labyrinthe#getArriveeY()
    */
    private int arriveeY;
    
    /**
    * Le tableau contenant toutes les cases du labyrinthe
    * 
    * @see Labyrinthe#getTerrain()
    * @see CaseImplementee
    */
    private Case[][] terrain;
    
    
    private Noeud noeudActuel;
    private Stack<Noeud> pile;
    private ArrayList<Noeud> tested;
    
    /// CONSTRUCTEUR
    
    /**
    * Constructeur Labyrinthe.
    * <p>
    * A la construction du labyrinthe, on appelle la méthode initFromFile pour lire les informations du fichier texte afin d'initialiser les attributs du Labyrinthe 
    * </p>
    * 
    * @param file
    *            Le fichier .txt que l'on a sélectionné au départ
    * 
    * @see Labyrinthe#tailleX
    * @see Labyrinthe#tailleY
    * @see Labyrinthe#departX
    * @see Labyrinthe#departY
    * @see Labyrinthe#arriveeX
    * @see Labyrinthe#arriveeY
    * @see Labyrinthe#posX
    * @see Labyrinthe#posY
    * @see Labyrinthe#terrain
    * @see Labyrinthe#initFromFile
    */
    public Labyrinthe(File file) throws FileNotFoundException
    {
        initFromFile(file); // La méthode initFromFile permet de lire les informations du fichier texte afin d'initaliser les attributs du labyrinthe
        
        // Stack
        pile = new Stack<Noeud>();
        
        // Array list
        tested = new ArrayList<Noeud>();
    }
    
    /// GETTERS
    
    /**
    * Retourne la longueur du labyrinthe.
    * 
    * @return La taille X du labyrinthe. 
    */
    public int getTailleX()
    {
        return tailleX;
    }
    
    /**
    * Retourne la largeur du labyrinthe.
    * 
    * @return La taille Y du labyrinthe. 
    */
    public int getTailleY()
    {
        return tailleY;
    }
    
    /**
    * Retourne le point de départ en X du labyrinthe.
    * 
    * @return Le point de départ en X du labyrinthe. 
    */
    public int getDepartX()
    {
        return departX;
    }
    
    /**
    * Retourne le point de départ en Y du labyrinthe.
    * 
    * @return Le point de départ en Y du labyrinthe. 
    */
    public int getDepartY()
    {
        return departY;
    }
    
    /**
    * Retourne le point d'arrivée en X du labyrinthe.
    * 
    * @return Le point d'arrivée en X du labyrinthe. 
    */
    public int getArriveeX()
    {
        return arriveeX;
    }
    
    /**
    * Retourne le point d'arrivée en Y du labyrinthe.
    * 
    * @return Le point d'arrivée en Y du labyrinthe. 
    */
    public int getArriveeY()
    {
        return arriveeY;
    }
    
    /**
    * Retourne la position actuelle en X dans le labyrinthe.
    * 
    * @return La position actuelle en X dans le labyrinthe. 
    */
    public int getCurrentPositionX()
    {
        return noeudActuel.posX;
    }
    
    /**
    * Retourne la position actuelle en Y dans le labyrinthe.
    * 
    * @return La position actuelle en Y dans le labyrinthe. 
    */
    public int getCurrentPositionY()
    {
        return noeudActuel.posY;
    }
    
    
    /**
    * Change la position actuelle en X dans le labyrinthe.
    */
    public void setCurrentPositionX(int posX)
    {
        noeudActuel.posX = posX;
    }
    
    /**
    * Change la position actuelle en Y dans le labyrinthe.
    */
    public void setCurrentPositionY(int posY)
    {
        noeudActuel.posY = posY;
    }
    
    /**
    * Retourne le tableau contenant toutes les cases du labyrinthe.
    * 
    * @return Le tableau contenant toutes les cases du labyrinthe. 
    */
    public Case[][] getTerrain()
    {
        return terrain;
    }
    
    
    /// METHODES
    
    /**
    * Initialise les attributs du labyrinthe en lisant le fichier texte.
    * 
    * @param file
    *            Le fichier texte sélectionné au départ.
    * 
    * @throws FileNotFoundException  Si jamais on n'a pas de fichier à passer en paramètre.
    * 
    */
    public void initFromFile(File file) throws FileNotFoundException
    {
        Scanner in = new Scanner(file); // Je lis le fichier
        String info;
        
        /// J'initialise tous les attributs du labyrinthe
        info = in.next(); // Accède au prochain élément dans le fichier texte (string)
        tailleX = parseInt(info); // on utilise un parseInt car l'élément lu dans le texte est un string et nous devons le convertir en entier
        info = in.next();
        tailleY = parseInt(info);
        info = in.next();
        departX = parseInt(info);
        info = in.next();
        departY = parseInt(info);
        info = in.next();
        arriveeX = parseInt(info);
        info = in.next();
        arriveeY = parseInt(info);
        
        
        noeudActuel = new Noeud(departX, departY);
        
        System.out.println(tailleX + " " + tailleY);
        
        terrain = new Case[tailleX][tailleY]; // On initialise les dimensions du labyrinthe avec les tailles récupérées précédemment
           
        String ligne;
        for(int j = 0; j<tailleX; j++) // Pour chaque ligne du fichier texte représentant une ligne du labyrinthe
        {
            ligne = in.next(); // On lit la ligne
            for(int i = 0; i<tailleY; i++) // Pour chaque caractère dans la ligne (correspondant à une case)
            {
                if(ligne.charAt(i)=='X') // Si on lit un 'X', alors il s'agit d'un mur et on le place dans le tableau de cases "terrain"
                {
                    Case new_case = new CaseMur(i,j);
                    terrain[j][i] = new_case;
                }
                if(ligne.charAt(i)=='_') // Si on lit un '_', alors il s'agit d'un trou et on le place dans le tableau de cases "terrain"
                {
                    Case new_case = new CaseTrou(i,j);
                    terrain[j][i] = new_case;
                }
                
            }
        }
        
        in.close(); // On ferme le fichier que l'on lit

    }
    
    /**
    * Se déplacer dans le labyrinthe grâce à une direction.
    * 
    * @param direction
    *            La direction saisie par l'utilisateur.
    * 
    * @throws ImpossibleMoveException  Si la case visée n'est pas accessible (mur ou en dehors du terrain).
    * 
    */
    public Boolean move(char direction) throws ImpossibleMoveException{
        Boolean ok = false;
        
        /// DEPLACEMENTS 
        switch(direction){ // En fonction de la direction passée en paramètre
            case 'q':
                // Gauche
                if(getCurrentPositionY() > 0){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX()][getCurrentPositionY() - 1].canMoveToCase()) // Si l'on peut se déplacer dans la case à gauche
                    {
                        noeudActuel.posY--; // On change la coordonnée actuelle en Y
                        System.out.println("==> Mouvement à gauche");
                        ok = true;
                    }
                    else{
                        throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                    }
                }
                else{
                    throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                }
                break;
            case 's':
                // Bas
                if(getCurrentPositionX() + 1 < getTailleX()){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX() + 1][getCurrentPositionY()].canMoveToCase()) // Si l'on peut se déplacer dans la case en bas
                    {
                        noeudActuel.posX++; // On change la coordonnée actuelle en X
                        System.out.println("==> Mouvement en bas");
                        ok = true;
                    }
                    else{
                        throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                    }
                }
                else{
                    throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                }
                break;
            case 'd':
                // Droite
                if(getCurrentPositionY() + 1 < getTailleY()){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX()][getCurrentPositionY() + 1].canMoveToCase()) // Si l'on peut se déplacer dans la case à droite
                    {
                        noeudActuel.posY++; // On change la coordonnée actuelle en Y
                        System.out.println("==> Mouvement à droite");
                        ok = true;
                    }
                    else{
                        throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                    }
                }
                else{
                    throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                }
                break;
            case 'z':
                // Haut
                if(getCurrentPositionX() > 0){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX() - 1][getCurrentPositionY()].canMoveToCase()) // Si l'on peut se déplacer dans la case en haut
                    {
                        noeudActuel.posX--; // On change la coordonnée actuelle en X
                        System.out.println("==> Mouvement en haut");
                        ok = true;
                    }
                    else{
                        throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                    }
                }
                else{
                    throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
                }
                break;
            default:
                throw new ImpossibleMoveException(); // Si l'on ne peut pas se déplacer dans la case visée, alors on a une exception
        }
        return ok;
    }
    
    /**
    * Se déplacer dans le labyrinthe grâce à une direction.
    * 
    * @param direction
    *            La direction saisie par l'utilisateur.
    * 
    * @throws ImpossibleMoveException  Si la case visée n'est pas accessible (mur ou en dehors du terrain).
    * 
    */
    public Boolean moveWithoutEx(char direction){
        Boolean ok = false;
        
        /// DEPLACEMENTS 
        switch(direction){ // En fonction de la direction passée en paramètre
            case 'q':
                // Gauche
                if(getCurrentPositionY() > 0){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX()][getCurrentPositionY() - 1].canMoveToCase()) // Si l'on peut se déplacer dans la case à gauche
                    {
                        noeudActuel.posY--; // On change la coordonnée actuelle en Y
                        System.out.println("==> Mouvement à gauche");
                        ok = true;
                    }
                    else{
                        System.out.println("Impossible - mur");
                    }
                }
                else{
                    System.out.println("Impossible - hors de la map");
                }
                break;
            case 's':
                // Bas
                if(getCurrentPositionX() + 1 < getTailleX()){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX() + 1][getCurrentPositionY()].canMoveToCase()) // Si l'on peut se déplacer dans la case en bas
                    {
                        noeudActuel.posX++; // On change la coordonnée actuelle en X
                        System.out.println("==> Mouvement en bas");
                        ok = true;
                    }
                    else{
                        System.out.println("Impossible - mur");
                    }
                }
                else{
                    System.out.println("Impossible - hors de la map");
                }
                break;
            case 'd':
                // Droite
                if(getCurrentPositionY() + 1 < getTailleY()){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX()][getCurrentPositionY() + 1].canMoveToCase()) // Si l'on peut se déplacer dans la case à droite
                    {
                        noeudActuel.posY++; // On change la coordonnée actuelle en Y
                        System.out.println("==> Mouvement à droite");
                        ok = true;
                    }
                    else{
                        System.out.println("Impossible - mur");
                    }
                }
                else{
                    System.out.println("Impossible - hors de la map");
                }
                break;
            case 'z':
                // Haut
                if(getCurrentPositionX() > 0){ // Si on est bien dans les limites du labyrinthe
                    if(terrain[getCurrentPositionX() - 1][getCurrentPositionY()].canMoveToCase()) // Si l'on peut se déplacer dans la case en haut
                    {
                        noeudActuel.posX--; // On change la coordonnée actuelle en X
                        System.out.println("==> Mouvement en haut");
                        ok = true;
                    }
                    else{
                        System.out.println("Impossible - mur");
                    }
                }
                else{
                    System.out.println("Impossible - hors de la map");
                }
                break;
            default:
                System.out.println("Impossible");
        }
        return ok;
    }
    
    /**
    * Se déplacer aléatoirement dans le labyrinthe.
    * 
    * 
    * @throws ImpossibleMoveException  Si la case visée n'est pas accessible (mur ou en dehors du terrain).
    * 
    */
    public void randomMove() throws ImpossibleMoveException
    {
        Random r = new Random();
        int aleatoire = r.nextInt(4); // On récupère un nombre aléatoire entre 0 et 3 inclus
        char direction = 'a';
        switch(aleatoire) // En fonction du nombre aléatoire récupéré, on obtient une direction
        {
            case 0:
                direction = 'q';
                break;
            case 1:
                direction = 's';
                break;
            case 2:
                direction = 'd';
                break;
            case 3:
                direction = 'z';
                break;
            default:
                System.out.println("Incorrect aleatoire");
                break;
        }
        
        if(move(direction)){}
        
    }
    
    /**
    * Se déplacer automatiquement en  dans le labyrinthe.
    * 
    * 
    * @throws ImpossibleMoveException  Si la case visée n'est pas accessible (mur ou en dehors du terrain).
    * 
    */
    public void autoMove() {
        Noeud begin = new Noeud(0, 0);
        pile.push(begin);
        Noeud current = pile.peek();
        
        Noeud nouv;
        Boolean found = false;
        
        while(!pile.empty()){
            current = pile.peek();
            
            noeudActuel.posX = current.posX;
            noeudActuel.posY = current.posY;
            //noeudActuel = current;
            tested.add(current);
            pile.pop();
            System.out.println("Tested " + tested.get(0).posX + " " + tested.get(0).posY);
            System.out.println("Current " + current.posX + " " + current.posY);
            
            if(current.posX == arriveeX && current.posY == arriveeY){
                System.out.println("Gagné !");
                break;
            }
            
            // pour chaque direction
            for(int dir = 0; dir < 4; dir++){
                System.out.println(dir);
                char cd;
                switch(dir){
                    case 0:
                        cd = 'z';
                        break;
                    case 1:
                        cd = 's';
                        break;
                    case 2:
                        cd = 'q';
                        break;
                    case 3:
                        cd = 'd';
                        break;
                    default:
                        cd = 'n';
                        break;
                }
                System.out.println(cd);
                // Si possible
                System.out.println("1Current " + current.posX + " " + current.posY);
                if(moveWithoutEx(cd)){
                    System.out.println("Moved");
                    System.out.println("2Current " + current.posX + " " + current.posY);
                    // Si pas testé
                    found = false;
                    for(int i = 0; i < tested.size(); i++){
                        if(tested.get(i).posX == noeudActuel.posX && tested.get(i).posY == noeudActuel.posY){
                            found = true;
                            System.out.println("trouvé : " + i);
                            System.out.println("Taille tested : " + tested.size());
                            System.out.println("Noeud actuel : " + noeudActuel.posX + " " + noeudActuel.posY);
                            System.out.println("Noeud actuel : " + tested.get(i).posX + " " + tested.get(i).posY);
                            break;
                        }
                    }
                    if(!found){
                        nouv = new Noeud(noeudActuel.posX, noeudActuel.posY);
                        pile.add(nouv);
                        System.out.println("Nouveau noeud");
                    }
                    noeudActuel.posX = current.posX;
                    noeudActuel.posY = current.posY;
                }
            }
        }
    }
    
    public void reInit(){
        noeudActuel.posX = departX;
        noeudActuel.posY = departY;
    }
    
}
