/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import modele.ImpossibleMoveException;
import modele.Labyrinthe;
import vue.LabyrintheGraphique;
import vue.Partie;
import static vue.Partie.afficherMenu;
import static vue.Partie.afficher_lab;
/**
 * Cette classe est le main: Elle permet de gérer la totalité du programme 
 * et implémente la boucle de jeu.
 * 
 * @see Labyrinthe
 * @author Philippe Ong
 */
public class TP1 {

    /**
     * @param lab the command line arguments
     */
    
    public static Labyrinthe charger_lab() throws FileNotFoundException{
        JFileChooser chooser = new JFileChooser(); // Le type JFileChooser permet d'ouvrir une fenêtre pour sélectionner un fichier à ouvrir
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        chooser.setFileFilter(filter);
        
        if(chooser.showOpenDialog(null)== JFileChooser.APPROVE_OPTION)
        {
            File selectedFile = chooser.getSelectedFile(); // J'implémente un objet de type File qui prend la valeur du fichier que tu viens de choisir
            if(selectedFile.exists())
            {
                Labyrinthe lab = new Labyrinthe(selectedFile);
        
                return lab;
            }
            else
            {
                System.out.println("Le fichier n'existe pas !! ");
                System.exit(0);
                return null;
            }
        }
        else
        {           
            System.exit(0);
            return null;
        }
        
    }
    
    /**
     * Cette procédure constitue la boucle de jeu
     * 
     * @param lab
     *              Labyrinthe chargé
     */
    
    public static void boucleJeu(Labyrinthe lab){
        // Variables
        Scanner sc = new Scanner(System.in);
        int choix = -1;
        int quitter = 3;
        // Direction pour bouger
        char dir = 'a';
        Temporal debut, fin;
        
        debut = Instant.now();

        do{
            while(choix == -1){
                lab.reInit();
                // Menu Console
                afficherMenu();
                do{
                    try{
                        choix = sc.nextInt();
                    }
                    catch(InputMismatchException e){
                        choix = -1;
                        sc.next();
                    }
                }while(choix < 0 || choix > 3);
                
                // Debut du jeu
                debut = Instant.now();
            }
                    
            // On passe au jeu
            if(choix != quitter){
                // Lancer la partie selon le mode de jeu choisi
                afficher_lab(lab);
                
                if(lab.getArriveeX() == lab.getCurrentPositionX() && lab.getArriveeY() == lab.getCurrentPositionY()){
                    
                    fin = Instant.now();
                    System.out.println("Vous Avez gagné !!!! Bravo !!");
                    System.out.println("Vous avez résolu le labyrinthe en " + Duration.between(debut, fin).toMillis() + " milisecondes.");
                    System.out.println("(~" + Duration.between(debut, fin).toMillis() /1000 + " secondes).");
                    
                    // Revenir au menu
                    choix = -1;
                }
                
                // Mode manuel
                if(choix == 0){
                    System.out.println("********** Que faire ?");
                    System.out.println("********** z, q, s ,d pour se déplacer");
                    System.out.print("********** p pour revenir au menu : ");
                    dir = sc.next().charAt(0);
                    if(dir == 'p'){
                        // Revenir au menu
                        choix = -1;
                    }
                    else {
                        // Mouvement du personnage
                        try{
                            if(lab.move(dir)){}
                        }
                        catch(ImpossibleMoveException e){
                            
                        }
                    }
                }
                
                // Mode aléatoire
                if(choix == 1){
                    // Mouvement du personnage
                    lab.autoMove();
                    
                    // Revenir au menu
                    choix = -1;
                }
                
                // Mode DFS
                if(choix == 2){
                    try{
                        lab.randomMove();
                    }
                    catch(ImpossibleMoveException e){

                    }
                }
            }
            else
                System.out.println("Au revoir !");
        }while(choix != quitter);
    }
    
    /**
     * Main du programme.
     * 
     * @throws FileNotFoundException
     *              Si le fichier n'est correctement chargé à l'endroit spécifié.
     */
    
    public static void main(String[] args) throws FileNotFoundException {
        // Mode de jeu graphique ou console
        Boolean modeGraphique = true;
        LabyrintheGraphique lg = null;
        
        // Chargement du labyrinthe
        Labyrinthe new_lab = charger_lab();
        
        if(modeGraphique == true){
            // Création du mode graphique
            lg = new LabyrintheGraphique("Lab LP", new_lab);
        }
        else {
            // Boucle de jeu
            boucleJeu(new_lab);
        }
        
    }
   
}
